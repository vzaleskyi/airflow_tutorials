 #!/usr/bin/python
'''
First task output current dag id and db table name in logs.
Next task extracts current bash user.
After that BranchPythonOperator takes dicision wich task to do depending on table existence.
If it exists it will insert a row of data and query the table in next tasks.
If not it will create a table, insert a row of data and query the table in next tasks.
'''
import logging
import uuid
from datetime import datetime
from airflow.models.dag import DAG
from  airflow.utils.helpers import chain
from airflow.models.connection import Connection
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.operators.postgres_custom import PostgreSQLCountRowsOperator


config = {
    'dag_1': {
        "schedule_interval": None,
        "table_name": 'table_1',
        "start_date": datetime(2020, 4, 10)       
    }, 
    'dag_2': {
        "schedule_interval": None,
        "table_name": 'table_2',
        "start_date": datetime(2020, 4, 10)       
    }, 
    'dag_3': {
        "schedule_interval": '@daily', 
        "table_name": 'table_3',
        "start_date": datetime(2020, 4, 10)       
    }, 
}

log = logging.getLogger(__name__)

def print_into_log(**kwargs):
    """ method to print process start """
    log.info('{} start processing tables in database: {}'.format(kwargs['dag_id'].upper(), kwargs['table_name'].upper()))


def check_table_exist(sql_to_check_table_exist, table_name):
    """ callable function to get schema name and after that check if table exist """ 
    hook = PostgresHook()
    query = hook.get_first(sql=sql_to_check_table_exist.format(table_name))
    print(query)
    if query:
        return 'dummy_task'
    else:
        return 'create_table'

def notifier(**kwargs):
    """ push current DAG run id """
    return kwargs['run_id'] + ' ended' 



for dag_id, conf_dict in config.items():

    with DAG(dag_id, default_args=dict(start_date=conf_dict['start_date']), 
             schedule_interval=conf_dict['schedule_interval'], catchup=False) as dag:
        
        print_log_op = PythonOperator(
            task_id='print_log',
            provide_context=True,
            python_callable=print_into_log,
            op_kwargs={'dag_id': dag_id ,'table_name': conf_dict['table_name']},
            dag=dag,
        )

        get_cur_user_op = BashOperator(
            task_id='get_cur_user',
            bash_command='whoami',
            xcom_push=True,
            dag=dag,
        )

        sql = """SELECT * FROM information_schema.tables 
                 WHERE table_name = '{}'"""

        check_table_exist_op = BranchPythonOperator(
            task_id='check_table', 
            dag=dag,
            python_callable=check_table_exist,
            op_kwargs=dict(sql_to_check_table_exist=sql, table_name=conf_dict['table_name'])
        )

        create_new_table = PostgresOperator(
            task_id='create_table',
            sql='''CREATE TABLE %s (
                custom_id integer NOT NULL, 
                user_id VARCHAR (50) NOT NULL,                
                t_stamp TIMESTAMP NOT NULL 
            );''' % (conf_dict['table_name'])
        )

        dummy_task_op = DummyOperator(
            task_id='dummy_task',
            dag=dag
        )

        insert_row_op = PostgresOperator(
            task_id='insert_row',
            sql="INSERT INTO %s VALUES(%s, '%s', '%s');" % (
                conf_dict['table_name'], 
                uuid.uuid4().int % 123456789, 
                "{{ ti.xcom_pull(task_ids='get_cur_user') }}",
                datetime.now().strftime("%m-%d-%Y %H:%M:%S")
            ),
            trigger_rule=TriggerRule.ALL_DONE            
        )

        query_table_op = PostgreSQLCountRowsOperator(
            task_id='query_the_table',
            table_name=conf_dict['table_name'],
        )

        chain(
            print_log_op, get_cur_user_op, check_table_exist_op, 
            [create_new_table, dummy_task_op], 
            insert_row_op, query_table_op
        )





