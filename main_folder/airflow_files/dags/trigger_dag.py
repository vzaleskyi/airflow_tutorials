 #!/usr/bin/python
'''
FileSensor wait for file ‘run’ according to specified path 
then if the file exists, TriggerDagRunOperator will trigger run for dag_3 in job_dag.py.
When dag_3 is done ExternalTaskSensor will validate TriggerDagRunOperator status 
and in case of success will execute print_res_op, remove_file_op, create_file_op tasks one by one.
'''
from datetime import datetime, timedelta
from os.path import join
from airflow.models.dag import DAG
from airflow.models import Variable
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.subdag_operator import SubDagOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.operators.python_operator import PythonOperator


file_path = '/usr/local/airflow/dags/'
file_name = 'new.txt'
FILEPATH = Variable.get("path_variable") or join(file_path, file_name)
DIRPATH = Variable.get("path_variable_2") or '/usr/local/airflow/dags/'

PARENT_DAG_NAME = 'parent_dag'
CHILD_DAG_NAME = 'child_dag'
DAG_TO_TRIGGER = 'dag_3'

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2020, 4, 10) 
}

def print_data(**context):
    """ output dag run id and context """
    run_id = context['ti'].xcom_pull(task_ids='query_the_table', key='run_id')
    print(f'DAG RUN ID: {run_id}', '\n', context)


def sub_dag(parent_dag_name, child_dag_name, start_date, schedule_interval):
    dag = DAG(
        '%s.%s' % (parent_dag_name, child_dag_name),
        schedule_interval=schedule_interval,
        start_date=start_date,
    )

    with dag:

        dag_execution_sensor = ExternalTaskSensor(
            task_id='execution_sensor',
            external_task_id='trigger',
            external_dag_id=PARENT_DAG_NAME,
        )

        print_res_op = PythonOperator(
            task_id='print_res',
            python_callable=print_data,
            provide_context=True,
            dag=dag,
        )

        remove_file_op = BashOperator(
            task_id='remove_file',
            bash_command='rm "{}"'.format(FILEPATH),
            dag=dag,
        )

        create_file_op = BashOperator(
            task_id='create_file',
            bash_command='touch {}'.format(join(DIRPATH,'finished_' + '{{ ts_nodash }}' + '.txt')),
            dag=dag,
        )

        dag_execution_sensor >> print_res_op >> remove_file_op >> create_file_op

    return dag


with DAG(dag_id=PARENT_DAG_NAME, default_args=default_args,
         schedule_interval=None, catchup=False) as main_dag:

    file_sensor = FileSensor(
        task_id='file_sensor',
        filepath=FILEPATH,
        poke_interval=5*60,
    )

    trigger_dag_op = TriggerDagRunOperator(
        task_id='trigger',
        trigger_dag_id=DAG_TO_TRIGGER,
    )

    sub_dag = SubDagOperator(
    subdag=sub_dag(PARENT_DAG_NAME, CHILD_DAG_NAME, default_args['start_date'],
                    main_dag.schedule_interval),
    task_id=CHILD_DAG_NAME,
    )

    file_sensor >> trigger_dag_op >> sub_dag
