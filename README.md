## Description
<p>
    This software implements dags represented on the pictures below.
    For more details, dive into the code in dags folder
</p>

## Visuals
![jobs dag](/img/job_dags.png)
<p>jobs dag</p>
![trigger dag](/img/trigger_dag.png)
<p>trigger dag</p>

## Installation
- Clone this repo
- move to the folder where docker-compose file is located
- run `docker-compose up postgres`
- run in other terminal window: `docker-compose up initdb` (we need it only once if we clean up DB and start all from scratch)
- run in another terminal window: `docker-compose up scheduler webserver`
- Check http://localhost:8080
- set up database connection http://localhost:8080/admin/connection/ 

## Connection
Edit "postgres_default" by setting this values:
- Host : postgres
- Schema : airflow
- Login : airflow
- Password : airflow
- Port: 5432

## Usage
Run the web service with docker: `docker-compose up`

Build the image: `docker-compose up -d --build`

Check http://localhost:8080/
